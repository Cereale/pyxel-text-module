import pyxel as px
from alphabet_data import ALPHABET_DATA
from warnings import warn


def advanced_text(x: int, y: int, s: str, col: int, size: int = 1) -> None:
    """
    Draw a string `s` of color `col` and size `size` at coordinates (`x`, `y`)

    Args:
        x (int): x coordinate
        y (int): y coordinate
        s (str): text string to draw
        col (int): text string color
        size (int, optional): size. Defaults to 1.
    """
    if size <= 1:
        px.text(x, y, s, col)
    else:
        for e, char in enumerate(s):
            if char not in ALPHABET_DATA.keys():
                warn(f"Character '{char}' not in dictionary. Ignoring...\n")
                continue
            for i in range(6):
                for j in range(4):
                    if ALPHABET_DATA[char][i][j]:
                        px.rect(
                            x + (j + 4 * e) * size,
                            y + i * size,
                            size, size, col,
                        )
