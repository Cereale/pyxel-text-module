import pyxel as px
from advanced_text import advanced_text
from string import printable


class App:
    def __init__(self):
        px.init(160, 160)
        px.run(self.update, self.draw)

    def update(self):
        pass

    def draw(self):
        px.cls(0)
        advanced_text(5, 0, "hello", 8, 1)
        advanced_text(5, 6, "hello", 9, 2)
        advanced_text(5, 18, "hello", 10, 3)
        advanced_text(5, 36, "hello", 11, 4)
        advanced_text(5, 60, "hello", 12, 5)
        advanced_text(5, 96, "hello", 13, 6)
        advanced_text(80 - 2 * px.frame_count, 135, printable, 14, 3)


App()
