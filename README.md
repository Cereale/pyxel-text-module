# Pyxel text module

This module provides the `advanced_text` function to draw text of custom size for the [pyxel game engine](https://github.com/kitao/pyxel):

- `advanced_text(x, y, s, col, size)`<br>
    Draw a string `s` of color `col` and size `size` at coordinates (`x`, `y`).

See `example.py` for example of use:

![Example](screenshots/showcase.gif)