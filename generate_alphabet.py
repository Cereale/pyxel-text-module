import pyxel as px
from string import printable
import sys


def generate_alphabet_data() -> dict:
    """
    Generates alphabet data dict from the pyxel font buffer.

    Returns:
        dict: dictionnary of 2D boolean array for each char
    """
    alphabet_data = dict()
    px.init(4, 6)
    pyxel_canvas = px.Image(4, 6)
    for char in printable:
        pyxel_canvas.text(0, 0, char, 7)
        char_data = []
        for i in range(6):  # char height is 6 pixels
            char_data.append([])
            for j in range(4):  # char width is 6 pixels
                if pyxel_canvas.pget(j, i) == 7:
                    char_data[i].append(True)
                else:
                    char_data[i].append(False)
        alphabet_data[char] = char_data.copy()
        pyxel_canvas.cls(0)
    return alphabet_data


if __name__ == "__main__":
    alphabet_data = generate_alphabet_data()

    with open("alphabet_data.py", "w") as file:
        file.write("ALPHABET_DATA = ")
    file.close()

    with open('alphabet_data.py', 'a') as sys.stdout:
        print(alphabet_data)
